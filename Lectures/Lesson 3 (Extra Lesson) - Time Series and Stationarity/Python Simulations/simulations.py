from itertools import cycle
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
import numpy as np


def TWO_D():
    # Define parameters for the walk
    dims = 1
    step_n = 10000
    step_set = [-1, 0, 1]
    origin = np.zeros((1,dims))
    # Simulate steps in 1D
    step_shape = (step_n,dims)
    steps = np.random.choice(a=step_set, size=step_shape)
    path = np.concatenate([origin, steps]).cumsum(0)
    start = path[:1]
    stop = path[-1:]
    # Plot the path
    fig = plt.figure(figsize=(8,4),dpi=200)
    ax = fig.add_subplot(111)
    ax.scatter(np.arange(step_n+1), path, c='blue',alpha=0.25,s=0.05);
    ax.plot(path,c='blue',alpha=0.5,lw=0.5,ls=' — ',);
    ax.plot(0, start, c='red', marker='+')
    ax.plot(step_n, stop, c='black', marker='o')
    plt.title('1D Random Walk')
    plt.tight_layout(pad=0)
    plt.savefig('plots/random_walk_1d.png',dpi=250);
    
def THREE_D():
    # Define parameters for the walk
    dims = 3
    step_n = 1000
    step_set = [-1, 0, 1]
    origin = np.zeros((1,dims))
    # Simulate steps in 3D
    step_shape = (step_n,dims)
    steps = np.random.choice(a=step_set, size=step_shape)
    path = np.concatenate([origin, steps]).cumsum(0)
    start = path[:1]
    stop = path[-1:]
    # Plot the path
    fig = plt.figure(figsize=(10,10),dpi=200)
    ax = fig.add_subplot(111, projection='3d')
    ax.grid(False)
    ax.xaxis.pane.fill = ax.yaxis.pane.fill = ax.zaxis.pane.fill = False
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.scatter3D(path[:,0], path[:,1], path[:,2], 
                 c='blue', alpha=0.25,s=1)
    ax.plot3D(path[:,0], path[:,1], path[:,2], 
              c='blue', alpha=0.5, lw=0.5)
    ax.plot3D(start[:,0], start[:,1], start[:,2], 
              c='red', marker='+')
    ax.plot3D(stop[:,0], stop[:,1], stop[:,2], 
              c='black', marker='o')
    plt.title('3D Random Walk')
    plt.savefig('plots/random_walk_3d.png',dpi=250);
    
def THREE_D_MULTIPLE_RUNS():
    # Define parameters for the walk
    dims = 3
    n_runs = 10
    step_n = 1000
    step_set = [-1, 0 ,1]
    runs = np.arange(n_runs)
    step_shape = (step_n,dims)
    # Plot
    fig = plt.figure(figsize=(10,10),dpi=250)
    ax = fig.add_subplot(111, projection='3d')
    ax.grid(False)
    ax.xaxis.pane.fill = ax.yaxis.pane.fill = ax.zaxis.pane.fill = False
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
     
    for i, col in zip(runs, colors):
        # Simulate steps in 3D
        origin = np.random.randint(low=-10,high=10,size=(1,dims))
        steps = np.random.choice(a=step_set, size=step_shape)
        path = np.concatenate([origin, steps]).cumsum(0)
        start = path[:1]
        stop = path[-1:]
        # Plot the path
        ax.scatter3D(path[:,0], path[:,1], path[:,2],
                     c=col,alpha=0.15,s=1);
        ax.plot3D(path[:,0], path[:,1], path[:,2], 
                  c=col, alpha=0.25,lw=0.25)
        ax.plot3D(start[:,0], start[:,1], start[:,2],
                  c=col, marker='+')
        ax.plot3D(stop[:,0], stop[:,1], stop[:,2],
                  c=col, marker='o');
    
    plt.title('3D Random Walk - Multiple runs')
    plt.savefig('plots/random_walk_3d_multiple_runs.png',dpi=250);